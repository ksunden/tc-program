import argparse
import sys

from ._ft_series import ft_series
from ._config import parse_file

def tc_program():
    parser = argparse.ArgumentParser(description="Program an FT-Series temperature controller.")
    parser.add_argument("port", type=str, help="Serial port of the controller.")
    parser.add_argument("path", type=str, nargs="?", default=None, help="Path to configuration file, default to STDIN.")
    parser.add_argument("--address", "-a", type=int, default=1, help="Address to use when sending commands.")
    args = parser.parse_args()
    instr = ft_series(args.port, args.address)
    if args.path is None:
        f = sys.stdin
    else:
        f = open(args.path)

    parse_file(f, instr)


    instr.close()
    f.close()
