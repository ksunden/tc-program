import struct
import serial
from ._commands import name_to_code

class ft_series(object):

    @staticmethod
    def crc(bytestring):
        crc = 0xffff
        for b in bytestring:
            crc ^= b
            for i in range(8):
                if crc & 1:
                    crc >>= 1
                    crc ^= 0xa001
                else:
                    crc >>= 1
        return bytes([crc % 0x100, crc >> 8])


    def __init__(self, port, address=1):
        self.serial = serial.Serial(port)
        self.address = address
        self.protocol = "modbus"

    def _read_return(self, expected):
        bytes_ = self.serial.read(expected)
        #TODO: parse, check
        return struct.unpack(">h", bytes_[3:5])

    def close(self):
        self.serial.close()

    def read(self, code, read_only=False):
        code = name_to_code.get(code, code)
        out = bytes([self.address, 4 if read_only else 3, 0, code, 0, 1])
        self.serial.write(out + ft_series.crc(out))
        val = self._read_return(7)
        return val

    def write(self, code, value):
        code = name_to_code.get(code, code)
        out = bytes([self.address, 6, 0, code]) + struct.pack(">h", value)
        self.serial.write(out + ft_series.crc(out))
        val = self._read_return(8)
        return val



# TODO: parse decimal point values
