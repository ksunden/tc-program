import warnings

def parse_file(file_, instrument):
    for line in file_:
        if line[0] == "#":
            continue
        try:
            command, value = line.split()
            value = int(value)
        except ValueError:
            command, value = line.strip(), None

        try:
            if value is not None:
                instrument.write(command, value)

            print(command, instrument.read(command)[0])
        except (KeyError, TypeError):
            warnings.warn(f"Command {command} not found")
