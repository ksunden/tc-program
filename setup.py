from setuptools import setup, find_packages
import os

def read(fname):
    return open(os.path.join(here, fname)).read()


here = os.path.abspath(os.path.dirname(__file__))

extra_files = {"attune": ["VERSION"]}

with open(os.path.join(here, "tc_program", "VERSION")) as version_file:
    version = version_file.read().strip()

setup(
    name="tc-program",
    packages=find_packages(exclude=("tests", "tests.*")),
    package_data=extra_files,
    python_requires=">=3.6",
    setup_requires=["pytest-runner"],
    tests_require=["pytest", "pytest-cov"],
    install_requires=["pyserial"],
    extras_require={"dev": ["black", "pre-commit"]},
    version=version,
    description="Tool to program temperature controllers over FTBUS interface",
    author="Kyle Sunden",
    author_email="sunden@wisc.edu",
    license="MIT",
    url="https://gitlab.com/ksunden/tc-program",
    entry_points = {"console_scripts": ["tc-program=tc_program.__main__:tc_program"]},
    keywords="temperature",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "License :: OSI Approved :: MIT License",
        "Natural Language :: English",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
    ],
)
